# HDMI sound in linux

*How I fixed the sound fixed on Peach*
I wanted to make the third device on the hdmi card the default. The pch audio on the motherboard was instead.

Steps:
1. Find the right audio device by opening Kodi and trying each device. Find the address of this device with `aplay -l` Don't use -L because that won't show you the actual device numbers.
2. Test the audio. First try `speaker-test -c 2 -D plughw:0,8`. If `plughw` doesn't work try just `hw`, with your address.
3. Then try actual audio `aplay -D plughw:0,8 /usr/share/sounds/alsa/Front.wav`

If that worked you can alter the systems alsa settings. Edit `/usr/share/alsa/alsa.conf` and change the defaults to match your device address.

	Defaults.pcm.card 0
	Defaults.ctl.card 0
	Defaults.ctl.device 8

Reboot. Test if it worked by playing audio to default `aplay /usr/share/sounds/alsa/Front.wav`  or try `alsamixer`.