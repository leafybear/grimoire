# Gummiboot

In gummiboot menu
* `h` to show key bindings
* `d` will set the entry to default

#### Install
The installation will also attempt to compy the efi images into the right places in `/boot`

	pacman -S gummiboot

You'll need to edit/create these files. To dual boot make sure you have the windows one.

###### windows.conf
```
title      Windows 8.1
efi        /EFI/Microsoft/Boot/bootmgfw.efi
```

###### arch.conf
```
title          Arch Linux
linux          /vmlinuz-linux
initrd         /initramfs-linux.img
options        root=/dev/sda1 rw
```

###### loader.conf

```
timeout 4
default windows
```