# VNC
I wanted to use VNC to work on my linux box from a windows surface.
Remote desktop protocol is good, and already integrated into windows. But
All the information says vnc is faster and better developed. And that windows' rdp is newer than the one in linux, so rumor is they aren't entirely compatible.

On linux install `tigervnc`. Both yum and pacman have it. In windows, download any vnc client. TightVNC is the best. Open the client and connect to the linux computer's ip with a port of 5900 + n, with n as the instance number. Most likely this would be 5901.

## Setup

Setup a server (on the linux box):

1. `vncserver` Run initial setup as your user. This will make you set a vnc password for this user. And start the vnc server up.
2. `vncpasswd username` Set the password for any user
3. `vncserver kill :1` Stop the vnc server
4. `vncserver -geometry 100x600 -alwaysshared -dpi 96 :1` Start the vncserver with more options. The first instance of vncserver (:1) will be placed at a port or 5901. The second at 5902, etc.
You'll need an xinitrc like file at `~/.vnc/xstartup`. You can just copy in your xinitrc
5. `chmod 700 ~/.vnc` Change the permissions of `~/.vnc` so that it's more secure
