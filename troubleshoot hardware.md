# Hardware troubleshooting tools
*what they do and how to use them*

Intel's diagnostic tools website has a list of more tools to use for troubleshooting. These tools listed are the ones I use to check for hardware problems. They are mostly for windows.

## Identification

##### CPU-Z
CPU-Z is a freeware application that gathers information on some of the main devices of your system.

##### Speccy
Speccy gives you detailed statistics on every piece of hardware in your computer, including CPU, motherboard, RAM, graphics cards, hard disks, optical drives, and audio support. Speccy also adds the temperatures of the different components.

## Monitoring

##### [Open Hardware Monitor](https://http://openhardwaremonitor.org/)
 Open Hardware Monitor is a free open source application that monitors temperature sensors, fan speeds, voltages, load, and clock speeds of a computer. It supports most hardware monitoring chips found on motherboards.
[Download](http://openhardwaremonitor.org/downloads/)

##### Speedfan
SpeedFan monitors voltages, fan speeds, and temperatures in computers with hardware monitor chips. It can also access S.M.A.R.T. info and show hard disk temperatures.

##### PerfMonitor
PerfMonitor is a processor performance monitoring tool that allows you to track the frequency of four events chosen in a set of model-specific list. It can be used to identify the performance bottlenecks of a system, or to monitor a program for cache-miss rate or mispredicted branches. PerfMonitor can also be used as a hardware-level CPU comparison tool, comparing the key parameters of different CPUs running the same benchmark.

## Stress Test / Benchmarking

##### [Burn in Test](http://passmark.com/products/bit.htm)
A software tool that simultaneously stress tests all the main subsystems of a computer, for endurance, reliability, and stability.
[Download](http://passmark.com/download/bit_download.htm)

##### 3DMark
Benchmark programs that run intense tests that strain even the latest hardware. By comparing your benchmark score with millions of others you can find problems with hardware and software.

##### Intel Processor Diagnostic Tool
The Intel Processor Diagnostic Tool is a free application used to verify the functionality of an Intel microprocessor. The diagnostic checks for brand identification, verifies the processor operating frequency, tests specific processor features and does a stress test on the processor.

##### Memtest 86+
A free, standalone memory testing program.

##### Prime95
Used by PC enthusiasts and overclockers as a stability testing utility.

##### AIDA64
A complete PC diagnostics software utility that assists you while installing, optimizing or troubleshooting your computer. The utility provides all information about your system, from hardware devices and installed drivers, to operating system security and stability metrics.