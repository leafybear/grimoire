# Tiling Window Managers

keyboard-friendly alternatives to traditional desktops. These usually auto arrange application windows, driven by user-specified keybindings.


**Wmii** is the oldest and no longer actively developed. Very easy to get started with. It gives all the windows title bars, which most of the others won't.

**Awesome WM** is a lua based [[window manager]] with a large user community and high customisablity.

**Bspwm** is the newest tiling window manager and somewhat easier to set up for the first time, but depends on some additional components for panels and menus. It's enjoying a lot of popularity right now. Very scriptable.

### panels, bars, menus

* dzen2
* bar (bar ain't recursive)
* dmenu
* xdotool

### companion apps

* Ranger: the commandline file manager
* Weechat
* Irssi
* urxvt