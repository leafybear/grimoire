# Kodi Media Player

aka xbmc. Media center app for all platforms

### Useful key bindings 

| Key | Menus Effect   | Playback Effect |
|-|-|-|
| `\` | toggle fullscreen ||
| `s`     | system menu, to exit kodi ||
| `c`     | open context menu for the current item | show playlist |
| `bkspc` | go back one level, ultimately return to currently playing ||
| `f`     | - | fastfoward |
| `l`     | - | next subtitle |
| `i`     | show info ||
| `q`     | queue item |-|
| `r`     | - | rewind |
| `t`     | - | toggle subtitles |
| `w`     | toggle watch / unwatched |-|

[Kodi wiki controls list](http://kodi.wiki/view/Keyboard_controls)

## Linux install
Easy installs in Ubuntu and Archlinux. You'll need to compile it yourself for fedora, centos, debian.

## Sound
Make sure the right sound output device is selected in Kodi's system settings. Even if the rest of the OS has it's sound working, it might be set wrong in there.

## Screen Tearing
The default accelertion, SNA, for Intel HD chipsets can cause some screen tearing issues in Linux. I tried changing all the vertical sync settings inside Kodi but it didn't help. But you can fix that by adding a configuration file to your xorg server. Add this file to `/etc/X11/xorg.conf.d/20-intel.conf`

```
Section "Device"
   Identifier "Intel Graphics"
   Driver "intel"
   Option "TearFree" "true"
EndSection
```

[Archlinux wiki page on Intel Graphics troubleshooting](https://wiki.archlinux.org/index.php/Intel_graphics#Troubleshooting)