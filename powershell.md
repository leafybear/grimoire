# Powershell and Command Prompt

useful commands for fixing things in windows 8

## Permissions

#### Take Ownership
	takeown /f d: /r /d y

As administrator, take ownership of the drive `d:` and all it's contents. `/f` specifies the folder, `/r` is recursive, `/a` gives permissions to admin and not the current user, `/d y` will skip confirmations

## Files

#### Empty the recyle bin
	rd /s /q f:\$Recycle.bin