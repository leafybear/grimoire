# Emacs

|  Basics| |
|-|-|
| C-x C-f | “find“ file i.e. open/create a file in buffer |
| C-x C-s | save the file |
| C-x C-w | write the text to an alternate name |
| C-x C-v | find alternate file | 
| C-x i | insert file at cursor position |
| C-x b | create/switch buffers |
| C-x C-b | show buffer list |
| C-x k | kill buffer |
| C-z | suspend emacs |
| C-X C-c | close down emacs |

---

| Movement | |
|-|-|
|C-f| forward char |
|C-b| backward char |
|C-p| previous line |
|C-n| next line |
|M-f| forward one word |
|M-b| backward one word |
|C-a| beginning of line |
|C-e| end of line |
|C-v| one page up |
|M-v| scroll down one page |
|M-<| beginning of text |
|M->| end of text |

---

| Editing | |
|-|-|
| M-n | repeat the following command n times |
| C-u | repeat the following command 4 times |
| C-u | n repeat n times |
| C-d | delete a char |
| M-d | delete word |
| M-Del | delete word backwards |
| C-k | kill line |
| C-Space | Set beginning mark (for region marking for example) |
| C-W | “kill“ (delete) the marked region region |
| M-W | copy the marked region |
| C-y | “yank“ (paste) the copied/killed region/line |
| M-y | yank earlier text (cycle through kill buffer) |
| C-x C-x | exchange cursor and mark |
| C-t | transpose two chars |
| M-t | transpose two words |
| C-x C-t | transpose lines |
| M-u | make letters uppercase in word from cursor position to end |
| M-c | simply make first letter in word uppercase
| M-l | opposite to M-u |
