# Ranger: file manager

*commandline file manager with pixmap support in X*

![](images/ranger.png)

## Install

Install `ranger` from Arch reps. Optionally, get dependencies that add more functionality:

  * `atool` for archives
  * `highlight` for syntax highlighting
  * `libcaca` (img2txt) for image previews in ASCII
  * `lynx`, `w3m` or `elinks` for HTML
  * `mediainfo` or perl-image-exiftool for media file information
  * `poppler` (pdftotext) for PDF
  * `transmission-cli` for BitTorrent information
  * `w3m` for image previews

So to install the works:

`sudo pacman -S ranger atool highlight libcaca lynx mediainfo poppler transmission-cli w3m`

## Config

Copy starting config files to you home `.config` folder by running:

`ranger --copy-config=all`

You'll now have:
  * `rc.conf` - startup commands and key bindings
  * `commands.py` - commands which are launched with :
  * `rifle.conf` - applications used when a given type of file is launched.

### rifle.conf
Set default apps for opening files by adding to the `rifle.conf`. Each line is formatted

`ext [extension] = [program + options] "$@"`

So to open all jpegs with `feh`, resizing them to window size, black background and name printed on top-left corner, you would add

`ext jpg = feh -d -. -B black "$@"`

## Keybindings

![](images/ranger cheatsheet.png)

##### navigation
|  |  |
| - | - |
| j | move down |
| k | move up |
| h | go to parent |
| gg | go to top |
| G | go to bottom |
| ctl f | page down |
| ctl b | page up |
| J | half Page down |
| K | half page up |
| H | step back in history |
| L | step foward in history |
| gh | cd ~/ |
| gm | cd /media |
| ge | cd /etc |
| gd | cd /dev |
| gr | cd / |
| gM | cd /mnt |
| go | cd /opt |
| gv | cd /var |
| m ? | create new bookmark ? |

##### working with files
| | |
|-|-|
| i | display file |
| I or E | open file |
| r | open with |
| o | change sort order |
| z | change settings |
| zh | view hidden file |
| R | reload list |
| space | select current file |
| t | tag current file |
| cw | rename current file |
| / | search for file |
| n | jump to next match |
| N | jump to previous match |
| yy | yank (copy) file |
| dd | cut file 
| del | delete file |
| V | deselect all |
| ya | add files to yanked |
| yr | remove files from yanked list |

##### tabbed browsing
|  |  |
| - | - |
| ctl n | create new tab |
| ctl w | close current tab |
| tab | next tab |
| shift tab | previous tab |

##### shell commands
|  |  |
| - | - |
| ! | execute shell command |
| : | execute ranger command |
| du | measure current directory disk usage |
| chmod | change permissions of current file |

##### shell command flags
|  |  |
| - | - |
| -s | discard output |
| -d | detach proces and run in background |
| -p | return output to ranger's file pager |
| -w | don't return immediately, wait for enter key |
| -c | perform command on current file, not selected |

##### shell command placeholders
|  |  |
| - | - |
| %f | substitute highlighted file |
| %d | current directory |
| %s | currently selected file |
| %t | currently tagged file |

## example commands
* `:bulkrename %s` change the names of multiple files