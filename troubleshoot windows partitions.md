# Windows partition trouble

## can't install to the partition you choose
1. put your win 8 USB in and boot it
2. Go to Repair Your Computer
3. In the RE (recovery environment) choose command prompt and type following commands:
`diskpart`
`list disk`
it will show the list of your drives, with the information status, total space, and free space. Status would probably be "invalid" and free space would be "0 bytes". Next,
`select disk <disk number>`
disk number = as listed in previous command, normally 0
`clean`
now for verification of disk status and free space type
`list disk`
the status should be "online" and free space should be "disk size"
`exit`

4. now restart the machine and boot from the win8 again
proceed and at Where to Install Windows you should be able to view the hard disk drive by it's type.