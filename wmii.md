# Wmii
*titling window manager*

Wmii can be configured basically or hardcore. A basic config goes in `~/.wmii/wmiirc_local` and contains theme details, key bindings, tag rules. A more hardcorde config uses the full wmiirc file which configures Wmii when it runs. This controls all aspects of the window manager (use with care). It goes in `~/.wmii/wmiirc`

I’ll break the config down into modules you can add into your config file. The basics can go in either file, but should be at the top of a full (hardcore) config.

Links
* [Wmii arch linux wiki](https://wiki.archlinux.org/index.php/Wmii) 
* [Wmii debian wiki](http://wiki.debian.org/Wmii)

#### Basic

Any options you don’t want to change can be left out of this file since it is a local config for the most basic options. If an option is missing it will use the system default.

#### Keybindings

You can set what key to use as the modifer (i like windows/command key) and what keys to use with it for basic navigation. Unless you have a weird keyboard, you should make it the windows key so that it won’t interfere with Maya (or others). 
  * Mod1 = Alt 
  * Mod2 = Ctl 
  * Mod4 = Windows/Apple key

```bash
# Configuration Variables
MODKEY=Mod4
UP=j
DOWN=k
LEFT=h
RIGHT=l
```

#### Themes

Here's a lot of my own hand-made themes. Each theme is a block of `export` commands in tuples. You can copy this whole chunk in if you comment out any themes you're not using. Or just copy one theme into your `wmiirc`

You can choose your own colors for each of these elements. They have to be in hex code format in groups of three between quotes. Colors tuples: `<text> <background> <border>` If you want to make your own, these are the possible values to set:

  * `WMII_NORMCOLORS` for unfocused windows
  * `WMII_FOCUSCOLORS` for the focused window
  * `WMII_BACKGROUND` for the solid background color
  * `WMII_URGENTCOLORS` for notifications
  * `WMII_SELCOLORS` for selected tags
  * `WMII_RBARCOLORS` for the status bar

![](images/wmii themes.png)

```bash
#    _    _                        
#  _| |_ | |_  ___ ._ _ _  ___  ___
#   | |  | . |/ ._>|   |/ ._><_-<
#   |_|  |_|_|___.|_|_|_|___./__/
#----------------------------------------------
# Colors tuples: "<text> <background> <border>"
# uncomment all the lines for the theme you want
# and comment out all the others. or just copy 1
#----------------------------------------------
# template
#export WMII_BACKGROUND='#'
#export WMII_NORMCOLORS='# # #'
#export WMII_FOCUS_COLORS='# # #'
#export WMII_URGENTCOLORS='# # #'
#export WMII_BARCOLORS='# # #'

# Jello
export WMII_BACKGROUND='#d4d4d4'
export WMII_FOCUSCOLORS='#3d3d3c #b6be59 #e1ed7f'
export WMII_NORMCOLORS='#a1a1a1 #404040 #1a1a1a'
export WMII_BARCOLORS='#3d3d3c #b6be59 #e1ed7f'
export WMII_URGENTCOLORS='#2a2928 #7ba539 #cbf968'

# DeviantArt-like
export WMII_BACKGROUND='#c7d2c2'
export WMII_NORMCOLORS='#ffffff #576862 #47584e'
export WMII_FOCUS_COLORS='#2e3732 #819742 #c0cf4e'
export WMII_URGENTCOLORS='#2e3732 #b5c33a #b5c33a'
export WMII_BARCOLORS='#2e3732 #d2ded2 #bfcb8d'

# Analog
export WMII_BACKGROUND='#3e383c'
export WMII_NORMCOLORS='#909090 #404040 #383838'
export WMII_FOCUS_COLORS='#4cd4e1 #383838 #383838'
export WMII_URGENTCOLORS='#000000 #a0cb62 #86be29'
export WMII_BARCOLORS='#000000 #cf5e8a #a37c92'

# Blueish
export WMII_BACKGROUND='#7a9ab1'
export WMII_NORMCOLORS='#e9e9e9 #2c2b29 #2c2b29'
export WMII_FOCUS_COLORS='#2c2b29 #90b3db #2c2b29'
export WMII_URGENTCOLORS='#000000 #a0cb62 #86be29'
export WMII_BARCOLORS='#e9e9e9 #5e6964 #515352'

# Fu
export WMII_BACKGROUND='#4f5459'
export WMII_NORMCOLORS='#ffffff #3e3e3e #333333'
export WMII_FOCUS_COLORS='#ff9966 #333333 #333333'
export WMII_URGENTCOLORS='#333333 #dea61c #dea61c'
export WMII_BARCOLORS='#333333 #a7afc8 #3d3d3d'

# Orange Ubuntu
export WMII_BACKGROUND='#231e1b'
export WMII_NORMCOLORS='#ffffff #3c3431 #1b1714'
export WMII_FOCUS_COLORS='#000000 #ff8100 #ff6801'
export WMII_URGENTCOLORS='#000000 #87bb01 #6a9201'
export WMII_BARCOLORS='#ffffff #1b1714 #1b1714'

# Clean Purple
export WMII_BACKGROUND='#5b3e6c'
export WMII_NORMCOLORS='#cdcdcd #1d1d1d #272727'
export WMII_FOCUS_COLORS='#1d1d1d #bebebe #a2a2a3'
export WMII_URGENTCOLORS='#1d1d1d #a281ac #8d6c9b'
export WMII_BARCOLORS='#1d1d1d #7c51a0 #421866'
```

Just copy in any of these theme blocks and change as desired. For the additional theme features in my themes to work, use the advanced config file at the bottom

#### Font and default term
You can set the font for window borders and the status line to any font available on your system.

`export WMII_FONT='xft:Sans-10'`

You should also set a default terminal to open with mod-enter. (Xterm, urxvt, gnome-terminal, kterm) If you’ve been using gnome, just leave this as gnome-terminal.

`export WMII_TERM="gnome-terminal`

#### Tag Rules

Wmii uses tags instead of actual workspaces to arrange your windows. A window can be assigned to show up on 1 tag or many. You can do this at any time as well as set defaults for applications you commonly use. In this config, we’ll name each tag with a number for te sake of simplicity, so 1,2,3 etc.

```bash
# Tagging Rules
wmiir write /tagrules < 1
/Thunderbird.*/ -> 1
/Relax.*/ -> 1
/Egg.*/ -> ~+2+3
/Buddy List.*/ -> ~+2+3
/wranglers.*/ -> ~+2+3
/term.*/ -> sel
/maya.*/ -> 3
/houdini.*/ -> 3
/gimp.*/ -> 3
/.* gedit/ -> ~
/.* File Manager/ -> ~
/MPlayer|VLC/ -> ~
/.*/ -> sel
/.*/ -> 1
!
```

This is just an example. I’ll explain a few lines and then you can edit it how you want. The top two and bottom three lines are required. Don’t change them. After that each line is in the format of ''/Search.*/ -> tag'' with each search being a window title you expect, in the form of a regular expression. Then there’s an assignment arrow (->) and the name of the tag to assign it to. Examples:

`/Thunderbird.*/ -> 1`
Always put Thunderbird on tag 1.

`/Egg.*/ -> ~+2+3`
Show Egg floating on tags 2 and 3. (~ is the floating layer and works like a tag)

`/MPlayer|VLC/ -> ~`
Open Mplayer or Vlc on the tag I’m looking at but make it float above the other windows.

#### Column Rules

This works just like the tag rules except you’re defining the default widths of columns on each tag.

```bash
# Column Rules
wmiir write /colrules < 60+40
/2/ -> 70+30
/3/ -> 100
/.*/ -> 58+42
!
```

This works as `/tag/ -> column1width + column2width` Just set these values to whatever you want. The last is the default for any other tags.

#### Advanced Configuration

You want an advanced config huh? Welcome aboard! Copy this file (it contains all the above mods) and save it to ''~/.wmii/wmiirc''

```ksh
#!/bin/ksh -f
# Configure wmii
wmiiscript=wmiirc # For wmii.sh
. wmii.sh


# KEYS
MODKEY=Mod4
UP=j
DOWN=k
LEFT=h
RIGHT=l

# Bars
noticetimeout=5
noticebar=/rbar/!notice

# Jello theme
export WMII_BACKGROUND='#d4d4d4'
export WMII_FOCUSCOLORS='#3d3d3c #b6be59 #e1ed7f'
export WMII_NORMCOLORS='#a1a1a1 #404040 #1a1a1a'
export WMII_BARCOLORS='#3d3d3c #b6be59 #e1ed7f'
export WMII_URGENTCOLORS='#2a2928 #7ba539 #cbf968'

export WMII_FONT='xft:Sans-10'
export WMII_TERM="gnome-terminal"

set -- $(echo $WMII_NORMCOLORS $WMII_FOCUSCOLORS $WMII_BARCOLORS $WMII_URGENTCOLORS)

if ! test -d "${WMII_CONFPATH%%:*}"; then
    mkdir "${WMII_CONFPATH%%:*}"
    res=$(wihack -type DIALOG xmessage -nearmouse -buttons Windows,Alt -print -fn $WMII_FONT 
          "Welcome to wmii,$wi_newline$wi_newline" 
          "Most of wmii's default key bindings make use of the$wi_newline" 
          "Windows key, or equivalent. For keyboards lacking such$wi_newline" 
          "a key, many users change this to the Alt key.$wi_newline$wi_newline" 
          "Which would you prefer?")
    [ "$res" = "Alt" ] && MODKEY=Mod1
    echo "MODKEY=$MODKEY" >"${WMII_CONFPATH%%:*}/wmiirc_local"
    chmod +x "${WMII_CONFPATH%%:*}/wmiirc_local"
fi

# Menu history
hist="${WMII_CONFPATH%%:*}/history"
histnum=5000

# Column Rules
wmiir write /colrules < 60+40
/2/ -> 70+30
/3/ -> 100+40+60
/.*/ -> 58+42
!

# Tagging Rules
wmiir write /tagrules < 1
/Thunderbird.*/ -> 1
/Relax.*/ -> 1
/Egg.*/ -> ~+2+3
/Buddy List.*/ -> ~+2+3
/wranglers.*/ -> ~+2+3
/term.*/ -> sel
/maya.*/ -> 3
/houdini.*/ -> 3
/gimp.*/ -> 3
/.* gedit/ -> ~
/.* File Manager/ -> ~
/MPlayer|VLC/ -> ~
/.*/ -> sel
/.*/ -> 1
!

# Status Bar Info
status () {
# Active Nodes, Memory, and Date
echo -n "mem" $(free -m | grep Mem: | awk '{printf "%2d%", ($3/$2)*100}') " | " $(date +"%b%e . %H:%M")
}

local_events() { true;}
wi_runconf -s wmiirc_local

echo $WMII_NORMCOLORS | wmiir create $noticebar

# Event processing
events() {
	cat <<'!'
# Events
Event CreateTag
	echo "$WMII_NORMCOLORS" "$@" | wmiir create "/lbar/$@"
Event DestroyTag
	wmiir remove "/lbar/$@"
Event FocusTag
	wmiir xwrite "/lbar/$@" "$WMII_BARCOLORS" "$@"
Event UnfocusTag
	wmiir xwrite "/lbar/$@" "$WMII_NORMCOLORS" "$@"
Event UrgentTag
	shift
	wmiir xwrite "/lbar/$@" "$WMII_URGENTCOLORS" "*$@"
Event NotUrgentTag
	shift
	wmiir xwrite "/lbar/$@" "$@"
Event LeftBarClick LeftBarDND
	shift
	wmiir xwrite /ctl view "$@"
Event Unresponsive
	{
		client=$1; shift
		msg="The following client is not responding. What would you like to do?$wi_newline"
		resp=$(wihack -transient $client 
			      xmessage -nearmouse -buttons Kill,Wait -print
			      -fn "${WMII_FONT%%,*}" "$msg $(wmiir read /client/sel/label)")
		if [ "$resp" = Kill ]; then
			wmiir xwrite /client/$client/ctl slay &
		fi
	}&
Event Notice
	wmiir xwrite $noticebar $wi_arg

	kill $xpid 2>/dev/null # Let's hope this isn't reused...
	{ sleep $noticetimeout; wmiir xwrite $noticebar ' '; }&
	xpid = $!

# Menus
Menu Client-3-Delete
	wmiir xwrite /client/$1/ctl kill
Menu Client-3-Kill
	wmiir xwrite /client/$1/ctl slay
Menu Client-3-Fullscreen
	wmiir xwrite /client/$1/ctl Fullscreen on
Event ClientMouseDown
	wi_fnmenu Client $2 $1 &

Menu LBar-3-Delete
	tag=$1; clients=$(wmiir read "/tag/$tag/index" | awk '/[^#]/{print $2}')
	for c in $clients; do
		if [ "$tag" = "$(wmiir read /client/$c/tags)" ]; then
			wmiir xwrite /client/$c/ctl kill
		else
			wmiir xwrite /client/$c/tags -$tag
		fi
		if [ "$tag" = "$(wi_seltag)" ]; then
			newtag=$(wi_tags | awk -v't='$tag '
				$1 == t { if(!l) getline l
					  print l
					  exit }
				{ l = $0 }')
			wmiir xwrite /ctl view $newtag
		fi
	done
Event LeftBarMouseDown
	wi_fnmenu LBar "$@" &

# Actions
Action showkeys
	echo "$KeysHelp" | xmessage -file - -fn ${WMII_FONT%%,*}
Action quit
	wmiir xwrite /ctl quit
Action exec
	wmiir xwrite /ctl exec "$@"
Action rehash
	wi_proglist $PATH >$progsfile
Action status
	set +xv
	if wmiir remove /rbar/status 2>/dev/null; then
		sleep 2
	fi
	echo "$WMII_BARCOLORS" | wmiir create /rbar/status
	while status | wmiir write /rbar/status; do
		sleep 1
	done

# Key Bindings
KeyGroup Moving around
Key $MODKEY-$LEFT   # Select the client to the left
	wmiir xwrite /tag/sel/ctl select left
Key $MODKEY-$RIGHT  # Select the client to the right
	wmiir xwrite /tag/sel/ctl select right
Key $MODKEY-$UP     # Select the client above
	wmiir xwrite /tag/sel/ctl select up
Key $MODKEY-$DOWN   # Select the client below
	wmiir xwrite /tag/sel/ctl select down

Key $MODKEY-space   # Toggle between floating and managed layers
	wmiir xwrite /tag/sel/ctl select toggle

KeyGroup Moving through stacks
Key $MODKEY-Control-$UP    # Select the stack above
	wmiir xwrite /tag/sel/ctl select up stack
Key $MODKEY-Control-$DOWN  # Select the stack below
	wmiir xwrite /tag/sel/ctl select down stack

KeyGroup Moving clients around
Key $MODKEY-Shift-$LEFT   # Move selected client to the left
	wmiir xwrite /tag/sel/ctl send sel left
Key $MODKEY-Shift-$RIGHT  # Move selected client to the right
	wmiir xwrite /tag/sel/ctl send sel right
Key $MODKEY-Shift-$UP     # Move selected client up
	wmiir xwrite /tag/sel/ctl send sel up
Key $MODKEY-Shift-$DOWN   # Move selected client down
	wmiir xwrite /tag/sel/ctl send sel down

Key $MODKEY-Shift-space   # Toggle selected client between floating and managed layers
	wmiir xwrite /tag/sel/ctl send sel toggle

KeyGroup Client actions
Key $MODKEY-f # Toggle selected client's fullsceen state
	wmiir xwrite /client/sel/ctl Fullscreen toggle
Key $MODKEY-Shift-c # Close client
	wmiir xwrite /client/sel/ctl kill

KeyGroup Changing column modes
Key $MODKEY-d # Set column to default mode
	wmiir xwrite /tag/sel/ctl colmode sel default-max
Key $MODKEY-s # Set column to stack mode
	wmiir xwrite /tag/sel/ctl colmode sel stack-max
Key $MODKEY-m # Set column to max mode
	wmiir xwrite /tag/sel/ctl colmode sel stack+max

KeyGroup Running programs
Key $MODKEY-a      # Open wmii actions menu
	action $(wi_actions | wimenu -h "${hist}.actions" -n $histnum) &
Key $MODKEY-p      # Open program menu
	eval wmiir setsid "$(wimenu -h "${hist}.progs" -n $histnum <$progsfile)" &

Key $MODKEY-Return # Launch a terminal
	eval wmiir setsid $WMII_TERM &

KeyGroup Other
Key $MODKEY-Control-t # Toggle all other key bindings
	case $(wmiir read /keys | wc -l | tr -d ' tn') in
	0|1)
		echo -n "$Keys" | wmiir write /keys
		wmiir xwrite /ctl grabmod $MODKEY;;
	*)
		wmiir xwrite /keys $MODKEY-Control-t
		wmiir xwrite /ctl grabmod Mod3;;
	esac

KeyGroup Tag actions
Key $MODKEY-t       # Change to another tag
	(tag=$(wi_tags | wimenu -h "${hist}.tags" -n 50) && wmiir xwrite /ctl view $tag) &
Key $MODKEY-Shift-t # Retag the selected client
	c=$(wi_selclient)
	(tag=$(wi_tags | wimenu -h "${hist}.tags" -n 50) && wmiir xwrite /client/$c/tags $tag) &
!
	for i in 0 1 2 3 4 5 6 7 8 9; do
		cat <<!--
Key $MODKEY-$i		 # Move to the numbered view
	wmiir xwrite /ctl view "$i"
Key $MODKEY-Shift-$i     # Retag selected client with the numbered tag
	wmiir xwrite /client/sel/tags "$i"
!
	done
}
wi_events events local_events

# WM Configuration
wmiir write /ctl <$progsfile &

# Setup Tag Bar
IFS="$wi_newline"
wmiir rm $(wmiir ls /lbar | sed 's,^,/lbar/,') >/dev/null
seltag=$(wmiir read /tag/sel/ctl | sed 1q)
unset IFS
wi_tags | while read tag
do
	if [ "$tag" = "$seltag" ]; then
		echo "$WMII_BARCOLORS" "$tag"
	else
		echo "$WMII_NORMCOLORS" "$tag"
	fi | wmiir create "/lbar/$tag"
done

wi_eventloop
```

Ok copy all that in. If you’ve followed this guide so far, you should be able to make all the same changes to this master config file. But be careful since it contains all of Wmii’s settings and must be correct for Wmii to load. This config file is located in ''?'' in case this doesn't work you, you can get a new copy and start from there.

#### Start up Scripts

Look for where it says `xsetroot` and `~/.wallpaper` in the file above. Those are simple wallpaper commands I use. You can put any of your own startup scritps or apps in that spot for Wmii to run them when it loads up.

And that's it! Have fun!