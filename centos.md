# CentOS

## Set up

### Install some tools


### Install Ranger
1. Clone the repository: `git clone git://git.savannah.nongnu.org/ranger.git`
2. Compile and install: `sudo make install`

### Copy the latest Dots
1. Clone the repository: `git clone git://github.com/fidgetfu/dots.git`

### Make a user for admin work
1. `useradd -m -g wheel -s /bin/zsh amy`
2. `passwd amy`

### Secure SSH

Edit the `/etc/ssh/sshd_config` file like this:

```
PermitRootLogin	no
AllowUsers amy
Protocol 2
PasswordAuthentication	no
```

Limit who can connect in `/etc/hosts.allow`

* For everyone add: `sshd: ALL`
* or local machines only: `sshd: 10.0.0.`

Restart SSH: `systemctl restart sshd`

### Secure the Firewall

Check the firewall state: `firewall-cmd --state`

---

## Yum

*Robust package manager for RHEL and CentOS.*
Hint: replace the caps word with your package, group, etc.

### Common tasks
* full update system `yum update`
* install a package `yum install PACKAGE`
* remove a package `yum remove PACKAGE`
* remove package and unneeded dependencies `yum autoremove PACKAGE`

###### PACKAGE GROUPS
* list package groups `yum grouplist`
* install a package group `yum groupinstall GROUP`
* update a package group `yum groupupdate GROUP`
* remove a package group `yum groupremove GROUP`

###### SEARCHING
* simple package search `yum list PACKAGE`
* wildcard package search `yum list "PACKAGE*"`
* search for packages `yum search NAME`
* provider package search `yum provides NAME*`

###### REPOS
* list enabled repositories `yum repolist`
* list all repositories `yum repolist all`

### Adding a custom repository using an RPM

Use the rpm commandline tool to install configuration files for a third-party repository, packaged as an rpm.

	rpm -Uhv http://apt.sw.be/redhat/el5/en/i386/rpmforge/RPMS/rpmforge-release-0.3.6-1.el5.rf.i386.rpm

### Adding a custom repository using .repo files

Descriptions of all the repositories available on the system live in `/etc/yum.d/repos/`.

To set up the karan.org Extras and Misc repos, download the repo files:
```
cd /etc/yum.repos.d/
wget http://centos.karan.org/
```

## You've been hacked, now what?

### Find the offending process
