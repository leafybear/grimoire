# Live disk resize

Live partitions can be resized using `fdisk` and `resize2fs`, as long as they begin at the same block and have the same type.

To expand a live partition, use `fdisk` to delete it and recreate. It MUST begin at the same block as before. Reboot so that the new partition table is read. Then run `resize2fs /dev/thePartition`
