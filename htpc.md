# Home Theatre PC

Files to save for reinstalling the server and setting back up.
```
  Apache
  |  /etc/httpd/conf/httpd.conf 
  BackupPC 
  |  /etc/backuppc/backuppc.users 
  |  /etc/backuppc/config.pl 
  Lighttpd 
  |  /etc/lighttpd/lighttpd.conf 
  |  /etc/lighttpd/passwd 
  PHP 
  |  /etc/php/php.ini 
  Samba 
  |  /etc/samba/smb.conf 
  SABNZBd 
  |  /opt/sabnzbd/sabnzbd.ini 
  SickBeard 
  |  /opt/sickbeard/config.ini 
  Haswell X11 driver settings 
  |  /etc/X11/xorg.conf.d/20-intel.conf
```


## Software
Linux has the best home theatre tools, but they're a little tricky to set up sometimes. These below are the best I've encountered thus far.
### Library

* **Calibre**: Ebook library software, collects metadata on ebooks and organises them into a file structure. Compatible with OPDS protocol.

* **Ubooquity**: Comic or ebook sharing web server, java based. Lets users read or download comics directly from library. Supports comics metadata.

### Playback

* **Kodi**: Aka XBMC, Kodi is our movie player of choice for home theatre setups. For individual files, MPV is reccomended. Kodi works with local and remote filesystems. Also a UPNP server.

* **MPV**: Standalone video player based on mplayer2. Full codec support for all encodings.

* **Mopidy**: Music serving daemon

* **Ncmpcpp**: Mpd compatible client for streaming audio or local use.

### Media retrieval daemons

These daemons are supposed to monitor your media and download new episodes as they become available. SABNZBD is a news group downloader. Sickbeard is an nzb searcher. It finds nzb files for episodes of TV and sends it to SABNZB which does the downloading. Honestly it doesn't seem all that helpful right now. And it was a bitch to set up correctly.

#### SABNZBD

Get the package from AUR. Files will be in /opt/sabnzbd. You can either run **sabnzbd** as a user or enable the systemd daemon by doing
<code>sudo systemctl enable sabnzbd</code>
If all goes well, you can visit **localhost:8080** in a web browser to do the initial setup.

##### Settings
Before you can do any more setup, you need to have some folder structure set up for files to download to. And also some permissions that will let other apps work with what SABNZBD creates. This is do-able but a little tricky.

You're file structure will be something like
```
  MainFolder/
    videos/
      tv/
      movies/
    incomplete-dl/
    nzb/
```
For now we just need to focus on the top level directory. Create the others later as you want them organised.
  - Create a new group for media apps to be in. `groupadd media_users`
  - Add the daemons to it. You may also want to add yourself to this group. `usermod -a -G media_users sabnzbd`
  - Create a folder that will be the top of your media file tree.
  - Change the owner of that folder `sudo chown -R youruser:media_users /path/to/theMediaFolder`
  - Change the mode of that folder so that user and group have equal permission, and that new files/folders inherit those permissions. The **2** set the group bit, inheriting group permissions in new files. `sudo chmod -R 2775 /path/to/theMediaFolder`

If everything went well, you should be able to `ls -l` and see something like this:
```
  total 36K
  drwxrwsr-x 6 amy     mediausers 4.0K Mar 31 00:48 .
  drwxr-xr-x 7 root    root       4.0K Mar 31 14:32 ..
  drwxrwsr-x 4 sabnzbd mediausers 4.0K Mar 31 00:49 downloads
  drwxrwsr-x 6 amy     mediausers 4.0K Mar 26 11:02 http
  drwxrwsr-x 2 amy     mediausers  16K Mar 24 13:57 lost+found
  drwxrwsr-x 5 amy     mediausers 4.0K Mar 31 15:26 video
```

Now you can go into SABNZBD and do some setup.

##### General Settings
Set the cleanup list to remove unnecessary files after extraction. For example, `.nfo, .sfv, .srr, .nzb`

##### Folder Settings
Set the download folders. Make sure to use the full path. Set the new file `chmod` setting to `2775`. The nzb folder is where it will watch for new downloads if you want to add them manually.

##### Server Settings
Make sure your news server connection settings are correct.

###### Categories Settings
You create categories here for media files so that the right type goes in the right folder. It also keeps priorities for each type.

###### Sorting Settings
Turn on TV sorting, to make sure files are named and filed correctly after download.

That should set you good to go. Grab an NZB and try it out.

### SickBeard

Sickbeard is a TV NZB search automaton. It scans your media folders and looks for missing/new episodes in the nzb search websites. If it finds anything you want, it will send it directly to SABNZBD (you need to give Sickbeard the API key for SABNZB). There's not much to set up here.