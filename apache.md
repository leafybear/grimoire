# Apache web server

## Installing

Install **apache**, **php** and **php-apache** using pacman.

		pacman -S apache php php-apache

## Configure

Edit the `/etc/httpd/conf/httpd.conf` to set your document root.

```
DocumentRoot "/srv/http"
<Directory "/srv/http">
```

* If you want to use `.htaccess` files in that directory, remove this line from the Directory settings: `AllowOverride None`
* To turn off your server's signature: `ServerSignature Off`
* To hide server information like Apache and PHP versions: `ServerTokens Prod`

### Testing

1. Test your apache configuration with the debugger by running

		apachectl configtest

2. Enable apache in **systemd**

		systemctl enable httpd

---

## Permissions for Document root

1. Create a new group: `groupadd webadmin`
2. Add your users to the group
	* `usermod -a -G webadmin user1`
	* `usermod -a -G webadmin user2`
3. Change ownership of the sites directory `chown root:webadmin /var/www/html/`
4. Change permissions of the sites directory `chmod 2775 /var/www/html/ -R`

Now anybody can read the files (including the apache user) but only root and webadmin can modify their contents.

---

## PHP 

Some additional modules are necessary for COPS opds websites. For any additional module, install the mod from pacman and enable the extension in `/etc/php/php.ini`

* php-gd
* php-sqlite
* php-intl

### Enable PHP

There's a compatibility problem between php5 and one of the default apache modules. Fix the conflict by replacing this:
`LoadModule mpm_event_module modules/mod_mpm_event.so`
With:
`LoadModule mpm_event_module modules/mod_mpm_event.so`

To enable PHP, add these lines to `/etc/httpd/conf/httpd.conf`:

1. Place `LoadModule php5_module modules/libphp5.so` in the **LoadModule** list anywhere after `LoadModule dir_module modules/mod_dir.so`
2. Place `Include conf/extra/php5_module.conf` at the end of the Include list

If your DocumentRoot is not /srv/http, add it to open_basedir in /etc/php/php.ini as such:

```
open_basedir=/srv/http/:/home/:/tmp/:/usr/share/pear/:/path/to/documentroot
```