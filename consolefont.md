# Console Font
Console fonts are kept in:
* ArchLinux: `/usr/share/kbd/consolefonts`
* CentOS: `/usr/lib/kbd/consolefonts`

Try setting the console font by running `setfont` on a font name without the extension.

	setfont Tamsyn10x20r

To change the font at boot, edit `/etc/vconsole.conf` (you may have to create it) and add a line

```
FONT=Tamsyn10x20r
```

### Issues

To have this font in the earliest part of the bootup process you would have to recompile the initramfs image with the `keymap` hook.

  - edit `/etc/mkinitcpio.conf`
  - add `keymap` to the list of hooks at the end
  - ... check [Mkinitcpio wiki](http://wiki.archlinux.org/index.php/Mkinitcpio)