# Pacman

How to make the most of Arch Linux's native package manager, PACMAN.

### Commands
|action | command |
|-|-|
| remove downloaded pkgs | `pacman -Scc` |
| update the system | `pacman -Syu` |
| update package lists | `pacman -Syy` |
| install from local pkg | `pacman -U xxx.pkg.tar.gz` |
| find a package | `pacman -Ss xxx` |
| find an installed pkg | `pacman -Qs xxx` |
| create a list of pkgs | `pacman -Qqe > pkglist` |
| install from a pkg list | `pacman -S $(cat pkglist)` |
| remove a pkg | `pacman -R xxx` |
| remove pkg and any otherwise unneeded deps | `pacman -Rs xxx` |
| find the pkg that owns file X | `pacman -Qo /path/to/X` |
| more information on a pkg | `pacman -Si xxx` |

oh and if it wasn't obvious, most of these take a pkg name as a final argument. that's what the "xxx" stands for just not the system ones of course.

---

### Downgrade Packages
New version not working for you? why not downgrade!! 

* [Arch Forum Topic](http://bbs.archlinux.org/viewtopic.php?id=60018)
* [Arch wiki howto](http://wiki.archlinux.org/index.php/Downgrade_packages#How_to_downgrade_a_package)


#### Old repositories
  * Arch Rollback Repo `http://arm.kh.nu/`
  * Schlunix old pkg repo `http://www.schlunix.org/archlinux/`

#### How to rollback

Install a package the usual way, allow it and all deps to install, and then use `pacman -U foo.tar.gz` to install the older version **OR** Install pkg and all deps from repo mirror archive

Specific example, how to downgrade to a working version of xorg-server

1. Install xorg group from pacman as per usual.
2. `pacman -Rd xf86-input-evdev`

3. `pacman -Rd xf86-video-vesa` These are incompaitble with the old ones, so we downgrade them too. `-Rd` for remove and ignore any dependencies. *(this is safe since we're experts o.O)*
4. `pacman -U xorg-server`
5. `pacman -U xf86-input-evdev`
6. `pacman -U xf86-video-vesa`
7. `pacman -U xf86-input-keyboard`
8. `pacman -U xf86-input-mouse`

Then you should be good. just make sure you put the names of all these downgraded pkgs in the ignore section of `/etc/pacman.conf`

```
IgnorePkgs = xf86-input-evdev xf86-input-keyboard xf86-input-mouse xf86-video-vesa 
IgnoreGroups = xorg-server
```

---

### Getting community packages from AUR

https://wiki.archlinux.org/index.php/AUR_helpers
