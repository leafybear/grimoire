# Crouton
*linux os-chroot and extended tools for chrome os*

## What is it?

>Crouton is a set of scripts that bundle up into an easy-to-use, Chromium OS-centric chroot generator. Currently Ubuntu and Debian are supported (using debootstrap behind the scenes)

### chroot?
> Like virtualization, chroots provide the guest OS with their own, segregated file system to run in, allowing applications to run in a different binary environment from the host OS. Unlike virtualization, you are not booting a second OS; instead, the guest OS is running using the Chromium OS system. The benefit to this is that there is zero speed penalty since everything is run natively, and you aren't wasting RAM to boot two OSes at the same time. The downside is that you must be running the correct chroot for your hardware, the software must be compatible with Chromium OS's kernel, and machine resources are inextricably tied between the host Chromium OS and the guest OS. What this means is that while the chroot cannot directly access files outside of its view, it can access all of your hardware devices, including the entire contents of memory. A root exploit in your guest OS will essentially have unfettered access to the rest of Chromium OS.

### Get Crouton
Download from [the Github page](https://github.com/dnschneid/crouton) or [here](https://goo.gl/fd3zc). It will do into `~/Downloads` which is a good place for it.

---

## Dev tools for Chromebook

Assuming your chromebook is in developer mode, you can install additional developer tools from within **Crosh**.

	sudo su -
	dev_install

This will give you way more linux tools inside your chromeos. Maybe you won't even need a crouton?

---
## Running Crouton

#### basics
* `sudo sh crouton -t xfce` would install an ubuntu based linux with xfce desktop
* `sudo enter-chroot startxfce4` start up the linux chroot
* `ctrl-alt-shift-back` and `ctrl-alt-shift-foward` cycle between chrome os and the chroot system

#### more commands
* List available targets `sh crouton -t list`
* List available distros `sh crouton -r list`
* Pick distro (**-r**) `sh crouton -r debian`
* Pick target (**-t**) `sh crouton -t cli-extra`
* Name the chroot (**-n**) `sh crouton -t cli-extra -n myterm`
* Set path for install the chroot (**-p**)
* Use encryption (**-e**) `sh crouton -e -t cli-extra`
* List available chroots (**-a**) `sh edit-crouton -a`
* Rename a chroot `sudo edit-chroot source -m destination`
* Delete a chroot `sudo delete-chroot chroot-name`
* Update a chroot `sudo sh crouton -u -n chrootname`
* Backup a chroot to SD `sudo edit-chroot -f /media/removable/SD\ Card/ -b chrootname`
* Backup a chroot to USB `sudo edit-chroot -f /media/removable/your_path_on_drive -b chrootname`
* Restore a back up `sudo edit-chroot -f /media/removable/your_path_on_drive -r chrootname`


#### chrome extension: one clipboard, one browser, one window
Installing the extension and its target gives you synchronized clipboards, the option of using Chromium OS to handle URLs, and allows chroots to create graphical sessions as Chromium OS windows.

1. Install the [crouton extension](https://goo.gl/OVQOEt).
2. Add the extension or `xiwi` version to your chroot.

---
## Recipes

#### i3wm
1. install X11 chroot: `sudo sh -e crouton -t x11 -n chrootname`
2. enter the chroot: `sudo enter-chroot -n chrootname`
3. install i3: `sudo apt-get install i3`
4. add `exec i3` to `~/.xinitrc`
5. launch i3 directly from crosh shell: `sudo enter-chroot xinit`

optionally, install `lxappearance` to alter themes without a lot of other packages.

#### Commandline only
1. Save a chunk of space by ditching X and just installing command-line tools using `-t core` or `-t cli-extra`
2. Enter the chroot in as many crosh shells as you want simultaneously using sudo enter-chroot
3. Use the Crosh Window extension to keep Chromium OS from eating standard keyboard shortcuts.

If you installed cli-extra, startcli will launch a new VT right into the chroot.