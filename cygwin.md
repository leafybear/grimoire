#  Cygwin integration

### Download

* [Cygwin](https://cygwin.com/install.html) (whatever tools you need)
* [Cmder](http://gooseberrycreative.com/cmder/) or [ConEmu](http://conemu.github.io/)

### Install Cygwin

### Add binaries to PATH

1. open advanced system settings
2. open Environment Variables
3. edit the PATH variable
4. add to the end, `;C:\cygwin64\bin` or wherever your cygwin install lies

### Get apt-cyg 

[Apt-Cyg](https://github.com/transcode-open/apt-cyg) is a script to let you add more cygwin packages easily without running the big installer again.*

1. open the cygwin terminal
2. run `lynx -source rawgit.com/transcode-open/apt-cyg/master/apt-cyg > apt-cyg`
3. and then `install apt-cyg /bin`

You can install new cygwin packages like this

    apt-cyg install nano

### Setup Cygwin in Cmder

![](images/cygwin integration cmder.png)

1. install the `chere` package from cygwin (e.g. `apt-cyg install chere`)
2. create a new task by clicking the + button in Startup > Tasks
3. Edit the name to something more recognisable (Cygwin)
4. Figure out the install directory of Cygwin and replace `<cygwin_dir>` with it in the following instructions
5. Change Task parameters to `/icon <cygwin_dir>\Cygwin-Terminal.ico`
6. Change Commands to `<cygwin_dir>/cygwin.bat -c "/bin/xhere /bin/bash.exe '%V'"`

Optionally set a hotkey to open cygwin in cmder
