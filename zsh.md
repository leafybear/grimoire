# ZSH

### Why use zsh?
Bash isn't bad. Zsh just does everything Bash does, plus.

* **Better completion handling**. Not only are completions command-specific, Zsh also adds the ability to navigate the completion list by hitting <Tab>.
* **Context based tab completion**
* **Spelling correction**. If you spell something wrong, Zsh can sometimes suggest a correction and re-run the command for you.
* **Right-hand prompts**. Zsh gives you the ability to add supplementary information to the right side of your prompt. I use this to display git information, but I've seen others use it for battery indicators.
* **Pattern matching/globbing** on alien steroids
```
ls *(.)            # list just regular files
ls *(/)            # list just directories
ls -ld *(/om[1,3]) # Show three newest directories. "om" orders by modification. "[1,3]" works like Python slice.
rm -i *(.L0)       # Remove zero length files, prompt for each file
ls *(^m0)          # Files not modified today.
emacs **/main.py   # Edit main.py, wherever it is in this directory tree. ** is great.
ls **/*(.x)        # List all executable files in this tree
ls *~*.*(.)        # List all files that does not have a dot in the filename
ls -l */**(Lk+100) # List all files larger than 100kb in this tree
ls DATA_[0-9](#c4,7).csv # List DATA_nnnn.csv to DATA_nnnnnnn.csv
```
* **Themeable prompts**
* **Loadable modules**
* **Global aliases**. Global aliases are words that can be used anywhere on the command line, thus you can give certain files, filters or pipes a short name.

```
alias -g L="|less" # Write L after a command to page through the output.
alias -g TL='| tail -20'
alias -g NUL="> /dev/null 2>&1" # You get the idea.
```
* **Syntax highlighting**
* **--**

---


### Config

* Change your shell: `chsh -s /bin/zsh`
* Edit config: `vim ~/.zshrc`
* Reload config: `source ~/.zshrc`

#### The completion system
The completion system is completely configurable and context dependent. To configure it we use the zstyle command:

	zstyle <context> <styles>


---

### Community Frameworks

* [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
* [prezto](https://github.com/sorin-ionescu/prezto)
* [zsh modules documentation](http://www.math.technion.ac.il/Site/computing/docs/zsh/zsh_21.html)