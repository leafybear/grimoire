# Customize Chromebook Chrosh Shell Environment

Requirement: Chromebook, Common Sense, Commandline Ablity, 1 hour of time

Dear developers with a spare Chromebook lets inject a little personalization into your Crosh shell with custom fonts, the solarized theme, and extra secure shell options.

Also, keep in mind that the terms `Chrosh`, `Chrosh Window`, and `Secure Shell` all refer to various versions and extentions built around the ChromeOS terminal. Settings that affect the ChromeOS terminal are global.

## Custom Fonts

If ChromeOS was a normal Linux distribution, adding fonts would be as easy as dropping a font file into the .fonts folder and changing a terminal setting. Sadly, we need to do a bit of work - some of it rather scary - to get custom fonts working with ChromeOS.

### Mounting Root Filesystem as Read-Write

Chromebook must be in **developer mode** and have **rootfs verification turned off** so that the root read only file system can be remounted as read-write. Once this is done, extra fonts can be added to ChromeOS's font folder.

**WARNING: Follow instructions very carefully. Failure to follow these instructions can result in a non-booting Chromebook. If you screw up you will need to restore your Chromebook to stock. You have been warned.**

1. Fire up the Crosh shell with `ctrl+alt+t` 

2. `sudo /usr/share/vboot/bin/make_dev_ssd.sh --remove_rootfs_verification`

3. The above command will fail and tell you to run with --partitions N flag at the end where N can be any number. Run the command again with the new flag.

4. `sudo crossystem dev_boot_signed_only=0`

5. The above command turns the verification check off. Because of step 2, the Chromebook will fail to boot if the verificaiton check is on.

6. `sudo mount -o remount,rw /` 

7. Reboot your Chromebook. The root file system is now mounted as read-write. 

Congrats. The dangerous part of the setup is done.

### Font Folder Location and Font Install

Fonts in .ttf format can be installed to the folder `/usr/share/fonts/croscore/`

Installing either the normal or powerline patched versions of your favorite monospaced fonts to the aforementioned folder is required for proper configuration of the font family settings in Secure Shell a bit latter.

[https://github.com/Lokaltog/powerline-fonts](https://github.com/Lokaltog/powerline-fonts)

I prefer the powerline patched version of Ubuntu Mono. Other mono space fonts - including Adobe's Source Code Pro - are great fonts for Crosh usage too!


### Add Required Chrome Extentions

To further customize our Crosh window, install both **Secure Shell** and the **Crosh Window** extentions from the Chrome Web Store.

* [Secure Shell](https://chrome.google.com/webstore/detail/secure-shell/pnhechapfaindjhompbnflcldabbghjo?hl=en)
* [Crosh Window](https://chrome.google.com/webstore/detail/crosh-window/nhbmpbdladcchdhkemlojfjdknjadhmh?hl=en-US)


### Sett Up Chrosh's Font-Family and Font-Size

ChromeOS `apps` menu > right click on the Secure Shell icon > select `options`

Change the terminal settings as you see fit. Pay special attention to:

#### font-family 

Keep in mind ChromeOS will only load the first avaliable font that you specified in the font-family field. If you wan to use a custom font make sure you added it as the first entry. For example, if using Ubuntu Mono add `"Ubuntu Mono", ` in front of the current font stack to properly add Ubuntu Mono

#### font-size

Since Ubuntu Mono is slighly smaller than the default font, you may want to bump the size up from 15 to a modestly larger 16 or 17.

## Solarized Color Theme

It's almost criminal how easy it is to install solarized for Crosh.

* Download a solarized prefs.js file. Two good options are:

  * [giraj/solarized-crosh](https://github.com/giraj/solarized-crosh)
  * [johnbender/prefs.js](https://gist.github.com/johnbender/5018685)

I'm including the code for the prefs.js file I used for convenience.

```
// Disable bold 
term_.prefs_.set('enable-bold', false) 

// Use this for Solarized Dark 
term_.prefs_.set('background-color', "#002b36");
term_.prefs_.set('foreground-color', "#839496");

// Solarized Colors
term_.prefs_.set('color-palette-overrides', [
  '#073642', 
  '#dc322f', 
  '#859900', 
  '#b58900', 
  '#268bd2', 
  '#d33682', 
  '#2aa198', 
  '#eee8d5', 
  '#002b36', 
  '#cb4b16', 
  '#586e75', 
  '#657b83', 
  '#839496', 
  '#6c71c4', 
  '#93a1a1', 
  '#fdf6e3'
]);
```

* Open the Chrome Javascript Console `ctrl+shift+j`
* Paste the contents of your solarized `prefs.js` file into the console
* Hit `enter`

## Further Customizations

Loading a Crosh Window based Ubuntu 14.04 Chroot is the best way to make use of the customizations.

* [dnschneid/crouton](https://github.com/dnschneid/crouton) with `sudo sh -e ~/Downloads/crouton -r trusty -t cli-extra`

For powerline patched font use in vim:

* add [bling/vim-airline](https://github.com/bling/vim-airline) to vim 
* edit your `.vimrc` to include `let g:airline_powerline_fonts = 1`

For powerline patched font use in tmux:

* Install `ZSH shell`, `oh-my-zsh`, and set the `zsh-theme` to `agnoster`