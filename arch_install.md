# Arch Linux Install
There's a lot to installing Archlinux, but instead of browsing through the full guide over at the wiki, I'm keeping up this page as a small reference guide. It's not going to be exhaustive but should be enough to help you through the process.

#### BIOS or (U)EFI
EFI (Extensible Firmware Interface) is a newer firmware than BIOS (Basic Input/Output System) for pcs. Macs and new pc motherboards tend to be UEFI.1) If you're computer is EFI/UEFI you should really use a linux boot method that is the same. Look in the bios of your PC to see what kind of boot options it has. “Legacy” is what EFI computers call BIOS boot mode.

Make sure:

* install iso is correct depth. 32 bit computers should use i686, 64 bit ones need x86_64
* boot media is created correctly. Most of the usb stick creating tools for windows don't make an efi boot stick or fail altogether. Use eitherUSBWriter.exe or dd
* boot usb media in efi mode. Your boot menu will probably let you choose between the legacy and efi versions of booting the stick. If you want to make an efi boot linux, make sure you boot the stick efi.

#### Making a USB bootable stick
There are 2 reliable ways to make bootable usb media: dd and USBWriter.exe. You can use dd on any system, technically. But on windowsUSBWriter.exe is the easiest.
The Arch syslinux installer uses the USB disk label to facilitate mounting the correct drive. The current version of UUI (1.9.5.2) sets the disk label to UUI, when Arch is expecting something else. You can easily fix this by right-clicking the USB drive icon, and clicking on Properties to change the label. For archlinux-2015.01.01-dual.iso, the label should be ARCH_201501. It should be clear what the label should be for other versions of the ISO, but in any case, Arch will tell you what the label needs to be if you attempt to boot from the USB with an incorrect label.

##### USBWriter.exe
Download USBWriter.exe from sourceforge. It has no installer, just run the program as is. Windows may complain that it is unsafe, but I haven't found it to be a problem. All you need is a Fat32 formatted usb stick with one partition. Select your usb target and install image and go. ![](images/usbwriter.png)

##### dd

In a linux system, you can write the img and partition table straight to the usb device in one move.
1. Find the device name `lsblk`
2. Write the img to that device
		
		dd if=Arch.iso of=/dev/sdX bs=4M

## Installation ##

### Disks and partitions
Create a partition somewhere for the linux install. Determine which partition is the windows EFI boot partition with `gdisk` to list the partitions on that disk.

	gdisk -l /dev/sda


- Set up a clean ext4 partition you can mount as your system root.
- Mount your system partition to /mnt
- Create /mnt/boot
- Mount the EFI parition there.

If you're replacing a previous linux install, you'll have to delete the vmlinuz and initramfs* files

Swap is optional these days.

### Network
if wired dhcp, do nothing for now

### Selecting package mirrors
Edit the mirror list for pacman and put the closest to you at the top

	nano /etc/pacman.d/mirrorlist

### Install base packages
	pacstrap -i /mnt base base-devel

### Generate Fstab
Generate the fstab file by UUID and store it. Make sure anything you need mounted (like swap) is set up before you do this.

	genfstab -U -p /mnt >> /mnt/etc/fstab

check that it's correct. if it's wrong, just edit it. don't rerun genfstab

	cat /mnt/etc/fstab

## Setup ##
### chroot into the new system
	arch-chroot /mnt /bin/bash

### Choose locale
uncomment the locales you need

	nano /etc/locale.gen
generate the locales
	
	locale-gen
set your default locale

	echo LANG=en_GB.UTF-8 > /etc/locale.conf
export it too

	export LANG=en_GB.UTF-8

### Keyboard layout
make any changes to the default keymap and console font here. 
to just change the font:

	echo FONT=Lat2-Terminus16 > /etc/vconsole.conf

### Timezone
set the default timezone, replacing Zone and Subzone with your location

	ln -sf /usr/share/zoneinfo/Zone/SubZone /etc/localtime

### Hardware clock
dual booting works best if windows and linux both assume the hardware clock is UTC. You need to change the registry in W8 to accomplish this.
set the hardware clock to UTC:

	hwclock --systohc --utc

### Set hostname
	echo my_hostname > /etc/hostname
add the same hostname to /etc/hosts after localhost

    #<ip-address> <hostname.domain.org> <hostname>
    127.0.0.1 localhost.localdomain localhost myhostname
    ::1   localhost.localdomain localhost myhostname

### Configure the network
for a single wired dhcp connection, replace `interface_name` with the ethernet interface
	
	systemctl enable dhcpcd@interface_name.service

### Root password
	passwd

### Bootloader install (UEFI)
	pacman -S dosfstools
You can optionally install gummiboot or else just use bootctl which comes with the base system.
To use bootctl, run:

	bootctl --path=$esp install
where `$esp` is your EFI partition path, usually `/boot`

Then create your entries for each boot option and edit the loader.conf.

----------

**Exit, unmount partitions and reboot the machine.**