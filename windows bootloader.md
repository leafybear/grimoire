# Windows boot loader

*troubleshooting the windows bootloader*

##### Grub is appearing instead of windows

You have to set things back how they were in two parts, with linux and with windows.
###### Boot a linux live usb (prefereably Arch)

1. find out which partition is the EFI system partition
2. mount it to `/mnt`
3. remove the `ubuntu` folder from `/mnt/EFI/`
4. look at the list of efi boot options
	`efibootmgr -v`
4. remove the ubuntu records with the `efibootmgr`
	`efibootmgr -b 5 -B`
6. reboot

###### Boot a windows usb

1. "Repair this computer"
2. "Troubleshoot"
3. "Advanced Tools"
4. "Command Prompt"
5. `bootrec /fixmbr`
6. reboot
The pc may ask for a bitlocker recovery key. It won't work unless you give it to it.