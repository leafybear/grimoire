# Github Pages
*html files for github project websites*

Create a branch called `gh-pages` that contains all the html files you want and push it to github or use their page generator.

To manually create a branch for pages:
1. `git clone https://github.com/user/repo.git`
2. `cd repo`
3. `git checkout --orphan gh-pages`
4. `git rm -rf .`
5. `echo "My GitHub Page" > index.html`
6. `git add .`
7. `git commit -a -m "First pages commit"`
8. `git push origin gh-pages`
