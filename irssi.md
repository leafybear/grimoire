# Irssi

Powerful and fully featured IRC client for all platforms. You can get it through cygwin for windows.

## Scripts

Put scripts in `$HOME/.irssi/scripts` and put a symlink to the ones you want to autorun in `$HOME/.irssi/scripts/autorun`

## Themes

Place in `$HOME/.irssi/`. Change your theme with `/set theme THEMENAME`

## Useful commands

Ignore parts/joins/quits in specified channel

	/ignore -channels #bspwm * JOINS PARTS QUITS NICKS

## Example config
You can edit the `$HOME/.irssi/config` file manually or change your settings inside IRSSI and save them to the config file. (This is easier for bare setups). An example of setting up from scratch inside a new irssi window:

```
/set theme arlyx
/nick arlyx
/server add -auto freenode irc.freenode.net 6667 PASSWORD
/channel add -auto #archlinux freenode
/bind meta-P key F1
```
(etc) you could bind other keys this way

```
/bind F1 change_window 1
/save
```

This sets the theme to `arlyx.theme` which is in `$HOME/.irssi/`. Changes our nick to **arlyx**. It adds Freenode as an autoconnecting IRC server and what our nick password is for that server. Also sets some channels to auto connect. Then it sets the F1 key to take use to window 1.

## Using with Screen

https://quadpoint.org/articles/irssi/
