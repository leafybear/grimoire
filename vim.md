# Vim: vi improved editor

##### hotkeys

* `v`	visual mode (for selecting text from cursor)
* `y`	yank (copy) text selected
* `esc`	cmd mode
* `w`	move foward by one word
* `p`	paste yanked (copied) text after cursor
* `i`	insert mode
