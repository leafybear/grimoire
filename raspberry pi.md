# Raspberry Pi

## Install Archlinux

SD card creation on an arch system. Replace sdX with the right device name for the sd card. From [ArchLinux-Arm Wiki: Raspberry Pi 2](http://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2)

1. `lsblk` Get a list of drives attached to the system: 

2. `fdisk /dev/sdX` Start fdisk to partition SD Card

3. At the fdisk prompt, delete old partitions and create a new one:
	* Type **o** to clear any partitions from the drive
	* Type **p** to list partitions. Should be none.
	* Type **n**, then **p** for primary, **1** for first partition of the drive, press **enter** to accept default sector, then type **+100M** for the last sector.
	* Type **t**, then **c** to set the first partition type to W95 FAT32.
	* Type **n**, then **p** for primary, **2** for the second partition, then press **enter** twice to accept sector defaults.
	* Type **w** to write the partition table and exit

4. Create the filesystems

	`mkfs.vfat /dev/sdX1`
	
	`mkfs.ext4 /dev/sdX2`

5. Mount them

	`mkdir sd-root`

	`mount /dev/sdX2 sd-root`

	`mkdir sd-root/boot`

	`mount /dev/sdx1 sd-root/boot`

6. Download and extract the filesystems

	`wget http://archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz`

	`bsdtar -xpf ArchLinuxARM-rpi-2-latest.tar.gz -C root`

	`sync`

7. Unmount the filesystems. Put the SD card in the rpi. Default accounts are alarm (password alarm) and root (password root).

---
## Initial setup

#### Correct overscanning
Edit `/boot/config.txt` and uncomment `#disable_overscan=1`

#### Change Hostname
	hostnamectl set-hostname NEWHOSTNAME

#### Change root password
	passwd

#### Change console font
Run `setfont Tamsyn10x20r` and edit `/etc/vconsole.conf`. Add a line that says `FONT=Tamsyn10x20r`. Specify any font that lives in `/usr/share/kbd/consolefonts/`.

#### New user
	useradd -m -G wheel -s /bin/bash amy

	passwd amy


---
## Arch Packages
Run a full system update in pacman before you install anything. This will fix any dependency problems that could come from the age of the system image.

	pacman -Syu

Next install the core development package so that you can use build tools. Accept the default options.

	pacman -S base-devel


#### Console fonts
  * **tamsyn-font** monospace bitmap font
  * **terminus-font** monospace bitmap font

#### Console apps
  * **ranger** file manager
  * **htop** advanced process manager
  * **vim** advanced editor
  * **irssi** IRC client
  * **git** version control system
  * **mutt** mail client
  * **zsh** alternative shell
  * **zsh-lovers** collection of tips, tricks and examples for Z shell
  * **zsh-db** debugger for zsh scripts
  * **zsh-syntax-highlighting** fish shell like syntax highlighting for zsh
  * **zsh-completions** additional completion definitions

#### Ranger preview libraries
  * **atool** archive handler
  * **highlight** source code highlighting
  * **libcaca** color ascii art image previews
  * **w3m** web browser
  * **mediainfo** technical and tag information about video/audio files
  * **poppler** pdf rendering
  * **perl-image-exiftool** read/write edif tags from raw files
  * **transmission-cli** bitTorrent support


#### X server
  * **xorg-server** X server
  * **xorg-xinit** front-end for executing window managers, without a display manager
  * **xorg-utils** 
  * **xorg-apps**
  * **xf86-video-fbdev** driver for raspberry pi video
  * **xorg-xrdb** user settings handler  for ''.Xresources''
  * **xorg-xfontsel** font browsing tool, sometimes useful
  * **xscreensaver** screen saver and locker for

#### Window manager
  * **bspwm** tiling window manager
  * **dmenu** run tool for starting programs in X
  * **rxvt-unicode** gui terminal app with unicode font support
  * **sxhkd** hotkey manager for X
  * **scrot** screenshot tool
  * **feh** image viewer
  * **conky** data display tool

#### X Fonts
  * **ttf-bitstream-vera** Bitstream vera fonts
  * **ttf-dejavu** family based on bitstream vera but with a wider range of characters
  * **ttf-linux-libertine** serif and sans-serif unicode fonts with wide glyph range
  * **ttf-oxygen** desktop font for KDE
  * **ttf-anonymous-pro** fixed width coding fonts
  * **ttf-sazanami** japanese fonts
  * **ttf-droid** fonts created by Google for Android
  * **ttf-fira-mono** mozilla's monospace font for firefox os
  * **ttf-fira-sans** mozilla's sans serif font for firefox os
  * **ttf-gentium** wide range of latin-based alphabets
  * **ttf-inconsolata** pretty terminal font
  * **ttf-liberation** red hat's liberation fonts
  * **ttf-symbola** font for unicode symbols
  * **ttf-ubuntu-font-family** Ubuntu font family

#### GUI apps group
  * **Kodi** streaming video player and media center (aka XBMC)
  * **mpv** movie player descended from player
  * **ncmpcpp** music player
  * **surf** very lightweight web browser for pi
  * **tabbed** a tab enclosure for use with surf and others
  * **chromium** Chrome for linux
  * **gpick** advanced color picking tool
  * **thunar** very functional but lightweight file manager
    * **tumbler** thumbnail support
    * **gvfs** remote filesystem support
    * **gvfs-smb** samba support

#### Gtk gui toolkit
If you're going to use any gui apps, chances are they will use Gtk to draw themselves. Enhancing with these packages will give you a nicer experience.

  * **gtk-chtheme** theme changing tool
  * **gtk2** Gtk 2 toolset
  * **gtk3** Gtk 3 toolset
  * **gtk-engines** various theme engines for Gtk
  * **faenza-icon-theme** Faenza icons
  * **tangerine-icon-theme** Tangerine icons
  * **gtk-xfce-engine** theme engine
  * **gtk-engine-unico** gtk3 theme engine
  * **gtk-aurora-engine** theme engine
  * **gtk-engine-murrine** theme engine
  * **gnome-themes-standard** theme collection
  * .... there are a very large number of additional pacakges from AUR of themes and engines. Too many to list!!

#### Other tools
  * **lm_sensors** sensor package for hardware temperature readings
  * **exfat-utils** exfat filesystem support
  * **ntfs-3g** ntfs filesystem support
  * **dosfstools** dos fs support

#### Networking and server
  * **samba** window file sharing service
  * **lighttpd** light weight web server

#### AUR: Arch User Repository
You'll have to get these packages from AUR since they come from the community, but they are pretty easy to install.

  * **compton** window composite effects
  * **lemonbar** (aka bar ain't recursive) a data displaying panel for X
  * **ttf-font-awesome** symbol font
  * **stlarch_font** symbol font
  * **mopidy** (requires AUR package python2-pykka) music server
  * **mopidy-spotify** (requires libspotify and pyspotify from AUR) spotify plugin for mopidy
  * **gtk-theme-murrine-collection** collection of gtk themes
  * **zukitwo-themes** gtk3/2 theme

To install, unpack the tarball and run these commands in the folder:
  - `makepkg -s`
  - `sudo pacman -U the-package-that-was-just-made.tar.gz`

It will tell you if any dependencies are missing or just install them if they're in pacman already.