# systemd

## journalctl
**systemctl** has it's own logging system, **journalctl**

* Show logs for a specific service `journalctl -u httpd`
* Show all messages from boot `journalctl -b`
* Follow new messages `journalctl -f`
* Show messages from last 20 minutes `journalctl --since "20 min ago"`
* Show all messages from a specific executable `journalctl /usr/lib/systemd/systemd`
* Follow new messages `journalctl _PID=1`
