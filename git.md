# Git: version control system

## Configuration

Git is configured by running `git config` commands at the command line or by editing `$HOME/.gitconfig`.
You can set configurations for the open repository by using any of these commands without `--global`. The name and email information is important for identifying the author of your commits in the repository. 

To set a config option at the command line:
	
	git config --global group.option "Value"

To set an option in the config file, add lines like

```
[group]
	option = "Value"
```

To open the config file in your editor, `git config -e`.
And to see config help, `git config --help`.

### Available options

There are many more. These are just basics.

| option | example value | what it does |
|-|-|-|
| user.name | foo bar | your name |
| user.email | foo@bar.com | your email |
| core.editor | emacs | default editor |
| core.excludesfile | $HOME/.gitignore_global | global git ignore file
| commit.template | $HOME/.gitmessage | default commit message |
| color.ui | true | enable color output |

### Special cases	

* Use TextWrangler as the editor: `git config --global core.editor "edit --wait --resume "$@""`
* User TextWrangler as diff editor: `git config --global diff.external $HOME/git-twdiff.sh`

```
## $HOME/.git-twdiff.sh:
#
# #!/bin/sh
# /usr/bin/twdiff --wait "$2" "$5"
```

### Additional files

#### Git message
```
## example of $HOME/.gitmessage.txt
# subject line
#
# what happened
#
# branch / version / build
```

#### Ignore Files
List the names of files to ignore in .gitignore in the project dir or use a global gitignore file, `~/.gitignore_global`

```
## Example lines of a gitignore:
# # don't track file in "my logs" folder
# my\ logs/*
# # but do track .gitignore
# !.gitignore
```

If you add tracked files to the ignore, you need to update GIT

	git update-index --assume-unchanged [filename(s)]

---

## Basics

Create a repository in the current dir
	
	git init

### Adding files

* Add a file `git add file.foo`
* Add all the files in cwd `git add --all`
* Also add all files `git add .`
* Add changes and deletions `git add -A`
* Add deletions `git add -u`
* Schedule files for deletion `git rm file.foo`
* Unstage all files in cwd `git reset`
* Unstage files up to current HEAD `git reset HEAD`

### Committing changes
* commit with comment `git commit -m “I changed stuff”`
* commit using editor for comment `git commit -v`
* change the comment of the last commit `git commit --amend`

### Get information
* Show recent commits `git log`
* Show log of changes, deletions, insertions `git log --stat`
* Show log with graph, tag names, and color `git log --color --graph --decorate`
* Show staged changes `git status`
* Show files in last commit `git ls-tree HEAD`
* Show all files in project tree `git ls-files`
* Show diff for last commit `git diff`
* Show diff for a file `git diff -- file.foo`
* Show diff between folder and HEAD `git diff --cached`

*HEAD always refers to the most recent commit, the top of the change tree. HEAD~1 is the one before the most recent, HEAD~2 is two before... etc.*

* Show the last few actions `git reflog`

### UNDO Changes
* Remove unstaged changes `git checkout -- .`     
* Remove unstaged changes `git checkout .`
* Checkout files from index, overwriting working tree `git checkout-index -a -f`
* Remove staged & unstaged changes `git reset --hard`
* Reset the current checkout of all changes `git checkout -f`
* Undo last commit, leaving files ready & staged `git reset --soft HEAD^`

### Revert a Commit
* Revert by checking out a previous commit `git checkout`

*In the git log each commit has a unique has code. use that here to refer to a specific commit.*

* Reverse a commit specified by hash and commit the result `git revert`
* Overwrite local file with most recent commit `git checkout file.foo`
* Get a previous version of a file `git checkout file.foo`

*Use the hash of the commit containing the version you want.*

### Stashing
*Stash your work so that you can switch branches without commiting*

* Stash current work dir `git stash save “comment”`
* Restore changes in your stash `git stash apply`
* Drop last stash `git stash clear`
* Show other stashes `git stash list`

### Branching
* Create a new branch of the project `git branch fooFeature`
* Delete a branch that has been merged `git branch -d branchFoo`
* Delete an unmerged branch `git branch -D branchFoo`
* Switch back to the master branch `git checkout master`
* List branches in the project `git branch`
* List remote branches `git branch -r`
* List all existing branches `git branch -a`
* Rename a branch by moving to a new name `git branch -m oldName newName`

### Merging
Example merge:
1. First switch back to the master `git checkout master`
2. Compare the two branches `git diff master branchFoo`
3. Merge your branch into the master `git merge branchFoo`

Other Merge commands
* Undo the beginning of a failed merge `git merge --abort`
* Merge with the strategy “theirs” `git merge otherBranch -s theirs`
* Undo changes to before that merge `git reset --hard ORIG_HEAD`
* Resolve merge conflicts `git mergetool -y`
* Add changes from  master by rebasing `git rebase master`

*This changes git’s own history. use with caution.*

### Tagging
Tagging is a useful way to mark particular commits.

* List tags in repository `git tag -l`
* Tag this commit with version number `git tag v1.0`

Now you can refer to this particular commit as though it were a branch

* Create tag with annotation `git tag -a v1.2 -m “My version”`

*annotated tags are less lightweight and better for sharing since they contain a lot more information.*

* Tag an older commit by hash tag `git tag -a v1.4`
* Delete a tag (not the commit) `git tag -d v1.4`

### Using a remote repository
* View existing remotes `git remote -v`
* Add a remote called origin to the repo `git remote add remoteName git@server.com:username/project.git`
* Push master changes to the remote `git push remoteName branch-name`
* Get any changes from remote servers `git pull`
* Push a mirror of the local repository to the remote `git push --mirror git@server.com:username/project.git`
* Push all branches on local to the remote `git push -u remoteName --all`
* Delete the branch from the remote `git push remoteName --delete branchFoo`
* Change the remote's URL `git remote set-url remoteName https://github.com/user/repo2.git`
* Remove the remote from repository `git remote rm remoteName`

---

## Advanced

**Clone into a non empty directory**:
You wouldn't do this except in unusual conditions, such as syncing the dot files in your home directory.

```
git init
git remote add git@gitlab.com/jedipixel/jedipixel-conf.git
git fetch gitlab
git clean -f
git checkout -t gitlab/master -f
```

#### Helpful Resources
* [Git Reference](http://gitref.org)     
* [Git Immersion](http://gitimmersion.com)
* [Git Book](http://git-scm.com/book/en/)
* [GitHUB hosting](http://github.com)
* [BitBucket hosting](http://bitbucket.org)