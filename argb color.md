# aRGB Color

Android uses Hex ARGB values, which are formatted as **#AARRGGBB**. That first pair of letters, the **AA**, represent the Alpha Channel. You must convert your decimal opacity values to a Hexdecimal value. Here are the steps:

1. Take your opacity as a decimal value and multiply it by 255. So, if you have a block that is 50% opaque the decimal value would be .5. For example: .5 x 255 = 127.5
2. The fraction won't convert to hex, so you must round your number up or down to the nearest whole number. For example: 127.5 rounds up to 128; 55.25 rounds down to 55.
3. Enter your decimal value in a decimal to hexadecimal converter, like this http://www.binaryhexconverter.com/decimal-to-hex-converter, and convert your values
4. If you only get back a single value, prefix it with a zero. For example, if you're trying to get 5% opacity and your going through this process you'll end up with the hex value of D. Add a zero in front of it so it appears as 0D.

| Percent | Code |
|-|-|
|100% | FF |
|95%	| F2 |
|90%	| E6 |
|85%	| D9 |
|80%	| CC |
|75%	| BF |
|70%	| B3 |
|65%	| A6 |
|60%	| 99 |
|55%	| 8C |
|50%	| 80 |
|45%	| 73 |
|40%	| 66 |
|35%	| 59 |
|30%	| 4D |
|25%	| 40 |
|20%	| 33 |
|15%	| 26 |
|10%	| 1A |
|5%	| 0D |
|0% | 00 |