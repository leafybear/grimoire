# Debian crouton
*with i3wm tiling manager, minimal UI and lots of cli apps*

## create the crouton
1. install X11 chroot: `sudo sh -e crouton -t x11 -n chrootname`
2. enter the chroot: `sudo enter-chroot -n chrootname`
3. install i3: `sudo apt-get install i3`
4. add `exec i3` to `~/.xinitrc`

## run the chroot

* enter at commandline: `sudo enter-chroot`
* enter in x11: `sudo enter-chroot xinit`

## packages to install

See packages installed manually

	aptitude search '!~M ~i'

Script to install all my packages at once

```
# Debian pkg installer
# cmdline
sudo apt-get install aptitude acpi build-essentials curl dstat dtrx git lftp mtr multitail netcat ngrep openssl pv ranger rsync silversearcher-ag tmux vim weechat wget
# x11
sudo apt-get install i3 conky scrot feh rxvt-unicode-256color
# fonts
sudo apt-get install fonts-fantasque-sans fonts-inconsolata fonts-mplus fonts-tuffy fonts-jura
```

What are the packages?
1. x11
	* rxvt-unicode-256color
	* conky
	* feh
	* i3
	* scrot
2. cmdline
	* aptitude
	* acpi
	* build-essentials
	* curl
	* dstat
	* dtrx
	* git
	* lftp
	* mtr
	* multitail
	* netcat
	* ngrep
	* openssl
	* pv
	* ranger
	* rsync
	* silversearcher-ag
	* tmux
	* vim
	* weechat
	* wget
	* zsh
3. fonts
	* fonts-fantasque-sans
	* fonts-inconsolata
	* fonts-mplus
	* fonts-tuffy
	* fonts-jura (maybe)