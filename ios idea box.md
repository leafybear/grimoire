## Name box refresh plan
First, replace all the graphics with the new ones. That means upping the Psd to retina size before saving them out.
Change the app type to master/detail
Copy in existing namebox code or work to alter current copy.

Changes to namebox
- master and delegate

## update ideas for other apps
# App Keys
- update visuals for iOS 7
- new icon
- thinner rows for keystrokes
- bolder more distinct colors - with a theme so they are informative
- launch images
- choose cleaner default font
- re evaluate mouse button font instead of lmb, mmb
# Name Box
- the refresh is great for iOS.
- add core data for names generated
- add a collection view with two tables that can show over the main window
- change the icon the flatter and more contrasty
# Tile Zen
- damn the rules, make the board fit the screen on each device
- make the menu obvious on iphone, extra space on iphone 5
- flatten the icon a little
- flatten the graphics a lot

## Tend edit idea
narrow down the app's functionality — sharpen the focus

	Two modes, lab book (journal) and field guide (plant list). The lab book should be readable in the list like you're looking through paper. only have to tap open to edit.
	Plant list is more like a pokedex. Keeping track of what plants you own is just an aspect of the plant list, not a whole list.

To change:
	Data model
	Ui
	Species list cells
	Diary list cells
	species detail view
	Diary detail view

Remove:
	Tags
	Care notes
	Plant views

Undecided:
	Pictures. If I can store them outside core data, then maybe I can sync just the core data stuff through icloud. That's a way to have both.

Features I'd like but don't have:
	Icloud sync of core data store
	Google image search window
	Search in list views

## New tend data model
Changes to the tend data model for second concept implementation:

Plant object
—Nickname
—Origin
—Date acquired
// Species (one to many)

Species object
—Lots of information as text
—Date modified for each time it gets changed
—Date added for creation date
—Link to its plant objects
Remove the tags