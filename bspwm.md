# Bspwm

A tiling window manager based on binary space partitioning
[Github page for Bspwm](https://github.com/baskerville/bspwm)

![](images/bspwm.png)

## Install

On Archlinux, install **bspwm** and **sxhkd** from official reps.  Sxhkd is a simple X hotkey daemon used to communicate with bspwm through bspc as well as launch your applications of choice.

`pacman -S bspwm sxhkd`

You may also want Compton for window effects (like compiz).

## Basic set up

To start at login add this to your `.xinitrc` (or `.xprofile`):

```bash
sxhkd &
exec bspwm
```

You need to config files to start using Bspwm and they don't get made automatically. Create a new `~/.config/bspwm/bspwmrc` that contains:

```
#! /bin/sh

bspc config border_width        2
bspc config window_gap         12

bspc config split_ratio         0.52
bspc config borderless_monocle  true
bspc config gapless_monocle     true
bspc config focus_by_distance   true

bspc monitor -d I II III IV V VI VII VIII IX X

bspc rule -a Gimp desktop=^8 follow=on floating=on
bspc rule -a Chromium desktop=^2
bspc rule -a mplayer2 floating=on
bspc rule -a Kupfer.py focus=on
bspc rule -a Screenkey manage=off</file>
```

Then create a file called `~/.config/sxhkd/sxhkdrc`

```
#
# bspwm hotkeys
#

super + alt + Escape
	bspc quit

super + w
	bspc window -c

super + t
	bspc desktop -l next

super + b
	bspc desktop -B

super + {s,f}
	bspc window -t {floating,fullscreen}

super + {grave,Tab}
	bspc {window,desktop} -f last

super + apostrophe
	bspc window -s last

super + {o,i}
	bspc control --record-history off; \
	bspc window {older,newer} -f; \
	bspc control --record-history on

super + y
	bspc window -w last.manual

super + m
	bspc window -s biggest

super + {_,shift + }{h,j,k,l}
	bspc window -{f,s} {left,down,up,right}

super + {_,shift + }c
	bspc window -f {next,prev}

super + {comma,period}
	bspc desktop -C {backward,forward}

super + bracket{left,right}
	bspc desktop -f {prev,next}

super + ctrl + {h,j,k,l}
	bspc window -p {left,down,up,right}

super + ctrl + {_,shift + }space
	bspc {window -p cancel,desktop -c}

super + alt + {h,j,k,l}
	bspc window -e {left -10,down +10,up -10,right +10}

super + alt + shift + {h,j,k,l}
	bspc window -e {right -10,up +10,down -10,left +10}

super + ctrl + {1-9}
	bspc window -r 0.{1-9}

super + {_,shift + }{1-9,0}
	bspc {desktop -f,window -d} ^{1-9,10}

~button1
	bspc pointer -g focus

super + button{1-3}
	bspc pointer -g {move,resize_side,resize_corner}

super + !button{1-3}
	bspc pointer -t %i %i

super + @button{1-3}
	bspc pointer -u

#
# wm independent hotkeys
#

super + Return
	urxvt

super + space
	dmenu_run

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd</file>
```

These two files are your window settings and key bindings, respectively. Make sure that `bspwmrc` is executable.

`chmod +x ~/.config/bspwm/bspwmrc`

If everything worked, you should see a blank screen!

## Add a bar

Lemonbar is the most common now, aka bar ain't recursive.