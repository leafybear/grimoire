# Regedit
*low-level windows modifications*

### to open regedit
Right click on windows button, click run. type `regedit`

### allow running powershell scripts
run this as administrator in the powershell

    Set-ExecutionPolicy RemoteSigned

### make shortcut names blank
name a shortcut with a blank by typing into it’s name field (using num pad) `255` while holding the alt key. use a different number for each icon since they have to be unique.

### correct number sorting
	HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer

1. create a new `DWORD`, name it `NoStrCmpLogical`
2. set it’s value to `1`
3. add folder to This PC list

### remove arrow overlay from shortcuts
	HEKY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Icons
1. If the key `Shell Icons` doesnt exist, create it.
2. add a new string `REG_SZ` named `29` with the value `C:\Windows\Blank.ico,0`

you need to have a blank icon file there. or use the path of an alternative icon you want to show. you can delete this key later if you want to use the default image.

### add a font to powershell
	HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Console\TrueTypeFont

Right-click in the panel on the right side and create a new string value. Name that value `0` or `00` or however many zeros you need to create a new key. That string’s value is the name of the font to add

### Hide OneDrive from Explorer navigation
	HKEY_CLASSES_ROOT\CLSID\{8E74D236-7F35-4720-B138-1FED0B85EA75}\ShellFolder
Take ownership of the `ShellFolder` key and change the Attribute value to `f090004d` instead of `f080004d`