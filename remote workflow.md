# Remote Workflow Recipes

*best practices for working remotely, windows or chromeos*

## Using SSH

> SSH keys always come in pairs, one private and the other public. The private key is known only to you and it should be safely guarded. By contrast, the public key can be shared freely with any SSH server to which you would like to connect.

The passphrase on a private key is for local system security only.

### Public/Private keys
On the remote, we'll make a key for the local host to use.

1. `ssh-keygen -t rsa -f localMachineName` if you specify a password, the key will be more secure locally but you'll have to type it in each time.
2. `touch ~/.ssh/authorized_keys` if it doesn't exist already
3. `cat localMachine.pub >> ~/.ssh/authorized_keys` to add the local machine to keys that are allowed to connect remotely
4. copy the new keys to the local machine's `~/.ssh` folder

Give the new keys the right permissions.

1. `chmod 700 ~/.ssh`
2. `chmod 644 ~/.ssh/id_rsa.pub`
3. `chmod 600 ~/.ssh/id_rsa`

### Copy the local key to a remote
If you want to copy your key from the local machine to the remote's authorized keys:

* `ssh-copy-id user@remote`
* `ssh-copy-id -i ~/.ssh/otherKeyName.pub user@remote`

### Multiple keys on one machine

You can have more than one ssh key on a single machine if you specify them in `~/.ssh/config`

```
Host Server1NameOrIP
  IdentitiesOnly yes
  IdentityFile ~/.ssh/id_rsa_SERVER1
  # CheckHostIP yes
  # Port 22
Host Server2NameOrIP
  IdentitiesOnly yes
  IdentityFile ~/.ssh/id_rsa_SERVER2
  # CheckHostIP no
  # Port 2177
ControlMaster auto
ControlPath /tmp/%r@%h:%p
```

When adding a remote key to the `authorized_keys` file, you can secure that machine's ability to run commands by specifying which ones it can run. Try adding something like `command="rsync" ssh-rsa key name`
to let a given key only run rsync.
### Secure the server to keys only

Edit `/etc/ssh/ssh_config` and change `PasswordAuthentication` to `no`. You'll have to restart `sshd`.

[arch linux ssh wiki page](https://wiki.archlinux.org/index.php/SSH_keys)

---

## Commandline sanity

### Important tools

* `Ranger`
* `irssi` or `Weechat`
* `Vim`
* `tmux`
* `htop`
* and of course the staples, `rsync` `git` `tail` `grep` `less` etc
* `mutt` for sending email with a shell script

Other tools I haven't tried
* `ncmpcpp`
* `siege`, http stress load tester
* **`ngrep`**, network tool, packet inspector
* `pv`, progress of data through a pipeline
* `dtrx`, extractor tool for archives
* `dstat`, versatile resource viewer
* `mtr` traceroute and ping in one tool
* `multitail` enhanced tail app
* `curl` data transfer tool
* `netcat` tcp/ip swiss army knife
* `nmap` offensive and defensive security scanner
* `openssl` encrypt files
* `lftp` better command-line ftp experience


### Dotfiles

1. Keep dotinclude list on a stick or cloud, to copy selectively with `rsync`
	* `rsync -avrzh --include-from=dotinclude 10.0.0.0:~/ ~/`
2. Or if all hosts have `git` use repository style management. Probably a better call if you want to sync changes around to machines. But they have to remember to pull.

### Chromebook linux crouton