# IRC

### channel commands

set a topic

	/topic some topic set

kick a user

	/kick nick some reason given

promote a nick to op (there's an o for each operator added)

	/mode #channel +o nick
	/mode #channel +ooo nick1 nick2 nick3

channel modes

	o: Operator
	s: Secret
	i: Invite only
	k: Keyword protected
	l: limited
	m: Moderated

##### setting the channel mode
You can combine these commands as well, like `+sl 20`
make a channel private or secret (unlisted)

	/mode #channel +p          (or +s)

make channel public

	/mode #channel -p          (or -s)

make channel invite only

	/mode #channel +i
	/invite buddy #channel

limit the number of people allowed to join

	/mode #channel +l 20

remove the limit

	/mode #channel -l

set a channe; password

	/mode #channel +k password
	/join #channel password

remove password

	/mode #channel -k password