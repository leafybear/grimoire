# Samba / CIFS (Windows File Sharing)

## Accessing from Linux
This is a great alternative to mounting a samba share and maintaining an open connection to it.

> Smbnetfs is a FUSE module that allows you to mount in a directory the SMB neighborhood of a machine. In this way, pretending to be a directory structure, you can access the servers and resources using the typical linux commands and applications like if we where in a file system. Smbnetfs maintains a connection for the mountpoint and only open new connections when you access a resource in a server and this connection is closed in a configurable time after the resource was accessed. *--from [Mounting a SMB/CIFS Share](http://fercaiscoding.com/?p=135)*

### SMBnetFS How to
1. Install `smbnetfs`. It's available from pacman.
2. Make a directory to use as your samba mount point, like `/home/amy/samba/`
3. Mount the samba neighborhood there: `smbnetfs mountpoint`
4. See all the samba shares in your network `ls mountpoint`
5. Authenticate as a user to access a share
  	* Either navigate directly to the share you want `cd mountpoint/resourcename/sharename/`
    * **OR** use authentication in the path `cd mountpoint/user:password@computer/resource`
    * **OR** use an authentication file containing the log in details for each share you'll access. Then you can use the resource without specifying your login `cd mountpoint/enterprise/PLEX`
      1. Create a `smbnetfs.auth` file next to the `smbnetfs.conf`. The file is in the format: `auth ip/resource user password`. Your file might look like 
```
auth enterprise/PLEX panda somepassword
auth 192.168.3.109/nyancat nyancat catalancat4
```