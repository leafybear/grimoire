# Mopdiy: music daemon

Mopidy is a music service for local playback and network streaming. Compatible with lots of audio clouds, like spotify

## Install and setup with Spotify

Packages to install from AUR (in order):

* python2-yenc
* python2-pykka
* mopidy
* libspotify
* pyspotify
* mopidy-spotify
